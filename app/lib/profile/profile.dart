import 'package:flutter/material.dart';

class Profile extends StatelessWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: const Text("Profile"),
      ),
      body: ListView(
        children: [
          const SizedBox(
            height: 25,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                width: 255,
                child: TextFormField(
                  decoration: const InputDecoration(label: Text("username")),
                  initialValue: "Fly",
                ),
              ),
              SizedBox(
                width: 255,
                child: TextFormField(
                  decoration: const InputDecoration(
                    label: Text("name"),
                    enabled: false,
                  ),
                  initialValue: "Zack",
                  enabled: false,
                ),
              ),
              SizedBox(
                width: 255,
                child: TextFormField(
                  decoration: const InputDecoration(
                    label: Text("surname"),
                  ),
                  initialValue: "lettuce",
                  enabled: false,
                ),
              ),
              SizedBox(
                width: 255,
                child: TextFormField(
                  decoration: const InputDecoration(label: Text("email")),
                  initialValue: "1234@gwa.com",
                ),
              ),
              SizedBox(
                width: 255,
                child: TextFormField(
                  obscureText: true,
                  decoration: const InputDecoration(label: Text("password")),
                  initialValue: "1234",
                ),
              ),
              SizedBox(
                width: 255,
                child: TextFormField(
                  obscureText: true,
                  decoration:
                      const InputDecoration(label: Text("confirm password")),
                  initialValue: "1234",
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(primary: Colors.green),
                onPressed: (() => Navigator.pushNamed(context, "/")),
                child: const Text("confirm"),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
