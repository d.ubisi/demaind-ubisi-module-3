import 'package:flutter/material.dart';

class About extends StatelessWidget {
  const About({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        
        title: const Text("About"),
        backgroundColor: Colors.green,
      ),
      body: const Center(
          child: Text(
        "Mtn App Accademy Is Awesome",
        style: TextStyle(color: Color.fromARGB(255, 255, 230, 0),fontSize: 22),
      )),
    );
  }
}
