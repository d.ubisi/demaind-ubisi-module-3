import 'package:flutter/material.dart';

class Year extends StatelessWidget {
  const Year({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Year"),
        backgroundColor: Colors.green,
      ),
      body: const Center(
          child: Text(
        "The year is 2022",
        style: TextStyle(color: Color.fromARGB(255, 28, 206, 22),fontSize: 22),
      )),
    );
  }
}
