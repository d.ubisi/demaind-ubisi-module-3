import 'package:app/2pages/about.dart';
import 'package:app/2pages/year.dart';
import 'package:app/auth/login.dart';
import 'package:app/auth/register.dart';
import 'package:app/home/home.dart';
import 'package:app/profile/profile.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: "/",
      routes: {
        "/": (context) => const Login(),
        "/register": (context) => const Register(),
        "/home": (context) => const MyHome(),
        "/about":(context) => const About(),
        "/year":(context) => const Year(),
        "/profile": (context) => const Profile()
      },
    );
  }
}
